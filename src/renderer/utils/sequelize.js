import Sequelize from 'sequelize'
import sqlConfig from '../config/sqlConfig'

export const sequelize = new Sequelize(sqlConfig.database, sqlConfig.name, sqlConfig.password, {
  host: sqlConfig.host,
  dialect: 'mysql', // 'mysql' | 'sqlite' | 'postgres' | 'mssql'
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
  // 仅限 SQLite
  // storage: 'static/database.sqlite'
})

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  })

export const LotteryTicket = sequelize.define('LotteryTicket', {
  type: Sequelize.STRING,
  typeId: {
    type: Sequelize.STRING,
    unique: 'compositeIndex'
  },
  typeName: Sequelize.STRING,
  qihao: Sequelize.STRING,
  datetime: Sequelize.DATE,
  numbers: Sequelize.STRING
})
